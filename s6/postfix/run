#!/usr/bin/env bash

[ "$DEBUG" == 'true' ] && set -x

cd /etc/postfix

# Copy default spool from cache
if [ ! "$(ls -A /var/spool/postfix)" ]; then
   cp -a /var/spool/postfix.cache/* /var/spool/postfix/
fi

if [ -z "$MAILNAME" ]; then
    echo "Error: MAILNAME not specified"
    exit 128
fi

if [ -z "$MYNETWORKS" ]; then
    MYNETWORKS='127.0.0.0/8, 10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16'
    echo "Warning: MYNETWORKS not specified, allowing all private IPs"
fi

# Configure Postfix
echo "Setting mailname to $MAILNAME"
echo $MAILNAME > /etc/mailname 
postconf -e myhostname="$MAILNAME"
postconf -e mydestination="$MAILNAME"
postconf -e mynetworks="$MYNETWORKS"
postconf -e relayhost="$RELAYHOST"
postconf -e smtp_fallback_relay="$FALLBACK_RELAYHOST"

# exit cleanly
trap "{ /usr/sbin/service postfix stop; }" EXIT

# Cleanup stale pids incase we don't exit cleanly
rm -f /var/spool/postfix/pid/*

# start postfix
/usr/sbin/service postfix start

sleep 10 # wait for startup

# watch for postfix exit
while kill -0 $(pidof master) 2>/dev/null; do 
	sleep 1
done 
